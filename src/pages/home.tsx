import React from 'react';
/* Components */
import {Nav} from '../components/Nav';

/* Instruments */
import {icons} from '../theme/icons/tag';

export const HomePage: React.FC = ()=>{
    return (
        <section className="layout">
            <section className ="hero">
                <div className = "title">
                    <h1>Типсы и триксы</h1>
                    <h2>React</h2>
                </div>
                <div className = "tags">
                    <span>
                        <icons.React/> React
                    </span>
                </div>
            </section>
            <Nav/>
        </section>
    )
}